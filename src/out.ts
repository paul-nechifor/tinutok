import buildCharacterSet from './buildCharacterSet';
import CharacterSet from './CharacterSet';
import Text from './Text';
import { textSize } from './constants';
import BezierSpline from './BezierSpline';
import BezierSplineWalker from './BezierSplineWalker';
import BezierSplineSpeedWalker from './BezierSplineSpeedWalker';

interface SvgOpts {
  string: string;
  characterSet?: CharacterSet;
  strokeWidth?: number;
  style?: string;
  scale?: number;
  padding?: number;
  wrapHeight?: number;
  noBackground?: boolean;
  addSize?: boolean;
}

interface SvgOut {
  svg: string;
  text: Text;
  width: number;
  height: number;
  spline: BezierSpline;
  startX: number;
  startY: number;
}

export function getSvg({
  string,
  characterSet,
  strokeWidth,
  style = '',
  scale = 4,
  padding = 10,
  wrapHeight = -1,
  noBackground = false,
  addSize = false,
}: SvgOpts): SvgOut {
  const givenHeight = (wrapHeight - 2 * padding) / scale - 2 * textSize;

  const set = characterSet || buildCharacterSet();

  const text = new Text(set, processString(string, set), givenHeight);
  const spline = text.getSpline();
  const startX = padding + textSize * scale;
  const startY = padding + textSize * scale;
  const path = spline.getRelativeSvgPath(startX, startY, scale);
  const height = scale * text.height + 2 * padding + 2 * scale * textSize;
  const width = text.width * scale + 2 * padding;
  const svg = [
    '<svg xmlns="http://www.w3.org/2000/svg" ',
    `viewBox="0 0 ${width} ${height}"`,
    addSize ? ` width="${width}" height="${height}"` : '',
    '>',
    noBackground
      ? ''
      : `<rect width="${width}" height="${height}" fill="#eee"/>`,
    `<path d="${path}" `,
    `style="fill: none; stroke: black; stroke-width: ${
      strokeWidth || scale
    }px; `,
    `${style}"/>`,
    '</svg>',
  ].join('');

  return {
    svg,
    text,
    width,
    height,
    spline,
    startX,
    startY,
  };
}

function processString(string: string, characterSet: CharacterSet): string {
  const characters = Object.keys(characterSet.characters);
  const letters = string
    .toLowerCase()
    .replace(/â/g, 'î')
    .split('')
    .filter((x) => x === ' ' || characters.includes(x))
    .join('');
  return letters;
}

interface DrawOpts {
  canvas: HTMLCanvasElement;
  string: string;
  characterSet?: CharacterSet;
  scale?: number;
  padding?: number;
  wrapHeight?: number;
  speedAdvance?: number;
  speedSteps?: number;
  nibWidth?: number;
  nibHeight?: number;
  showPath?: boolean;
}

export function drawOnCanvas({
  canvas,
  string,
  characterSet,
  scale = 4,
  padding,
  wrapHeight,
  speedAdvance = 0.1,
  speedSteps = 5,
  nibWidth = 3,
  nibHeight = 1,
  showPath = true,
}: DrawOpts): () => void {
  const { width, height, spline, startX, startY } = getSvg({
    string,
    characterSet,
    scale,
    padding,
    wrapHeight,
  });
  const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
  if (!ctx) {
    throw new Error('No canvas.');
  }
  canvas.setAttribute('width', `${width}`);
  canvas.setAttribute('height', `${height}`);
  ctx.fillStyle = '#EEEEEE';
  ctx.fillRect(0, 0, width, height);

  if (showPath) {
    ctx.lineWidth = 1;
    spline.draw(ctx, startX, startY, scale);
  }

  const walker = new BezierSplineWalker(
    spline,
    speedAdvance,
    startX,
    startY,
    scale,
  );
  const sWalker = new BezierSplineSpeedWalker(walker);
  let morePoints = true;

  function doNextPoint() {
    if (morePoints) {
      setTimeout(doNextPoint, 0);
    }

    for (let i = 0; i < speedSteps && morePoints; i++) {
      const { x, y } = sWalker;
      const len = nibWidth;

      ctx.lineWidth = nibHeight;
      ctx.beginPath();
      ctx.moveTo(x - len, y);
      ctx.lineTo(x + len, y + len);
      ctx.stroke();
      morePoints = sWalker.advanceBy(speedAdvance);
    }
  }

  doNextPoint();

  return () => {
    morePoints = false;
  };
}
