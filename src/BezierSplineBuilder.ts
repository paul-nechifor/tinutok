import BezierSpline from './BezierSpline';
import { computeCubicCurveLength } from './util';

const LENGTH_COMPUTING_POINTS = 1000;

export default class BezierSplineBuilder {
  array: number[];

  constructor() {
    this.array = [];
  }

  build(): BezierSpline {
    const v = this.array;
    let length = 0;
    const lengths = [];
    let height = 0;

    for (let i = 0, k = 0, len = v.length; i < len; i += 6, k++) {
      lengths[k] = computeCubicCurveLength(
        LENGTH_COMPUTING_POINTS,
        0,
        0,
        v[i + 0],
        v[i + 1],
        v[i + 2],
        v[i + 3],
        v[i + 4],
        v[i + 5],
      );

      length += lengths[k];

      height += v[i + 5];
    }

    const typedArray = new Float64Array(v);
    const typedLengths = new Float64Array(lengths);

    return new BezierSpline(typedArray, length, typedLengths, height);
  }

  addCubic(
    c1x: number,
    c1y: number,
    c2x: number,
    c2y: number,
    tx: number,
    ty: number,
  ): void {
    this.array.push(c1x, c1y, c2x, c2y, tx, ty);
  }

  addMatchingCubic(c2x: number, c2y: number, tx: number, ty: number): void {
    if (this.array.length === 0) {
      throw new Error('Cannot be first');
    }

    const v = this.array;
    const n = v.length;
    v.push(v[n - 2] - v[n - 4], v[n - 1] - v[n - 3], c2x, c2y, tx, ty);
  }

  // From: http://stackoverflow.com/questions/3162645
  addQuadratic(cx: number, cy: number, tx: number, ty: number): void {
    const f = 2.0 / 3.0;
    this.array.push(
      f * cx,
      f * cy,
      tx + f * (cx - tx),
      ty + f * (cy - ty),
      tx,
      ty,
    );
  }
}
