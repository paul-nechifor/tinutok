import BezierSplineBuilder from './BezierSplineBuilder';
import BezierSpline from './BezierSpline';
import { reverseCurve } from './util';

export default class Character {
  commands: string[];

  spline: BezierSpline | null;

  isInReverse: boolean;

  height: number;

  splinePoints: number;

  constructor(commands: string[]) {
    this.commands = commands;
    this.spline = null;
    this.isInReverse = false;
    this.height = 0;
    this.splinePoints = 0;

    this.splinePoints = commands.length;

    const builder = new BezierSplineBuilder();

    for (let i = 0, len = commands.length; i < len; i++) {
      const command = commands[i];
      const parts = command.split(/\s+/);
      const numbers: number[] = parts
        .slice(1, parts.length)
        .map((e) => Number(e));

      switch (parts[0]) {
        case 'c':
          builder.addCubic(
            ...(numbers as [number, number, number, number, number, number]),
          );
          break;
        case 'mc':
          builder.addMatchingCubic(
            ...(numbers as [number, number, number, number]),
          );
          break;
        case 'q':
          builder.addQuadratic(
            ...(numbers as [number, number, number, number]),
          );
          break;
        default:
          throw new Error('Forgot to implement it.');
          break;
      }
    }

    this.spline = builder.build();
    this.height = this.spline.height;
  }

  getInReverse(): Character {
    const c = new Character(this.commands.reverse().map(reverseCurve));
    c.isInReverse = true;
    return c;
  }

  draw(
    ctx: CanvasRenderingContext2D,
    startX: number,
    startY: number,
    scale: number,
  ): void {
    if (!this.spline) {
      throw new Error("There's no spline.");
    }
    this.spline.draw(ctx, startX, startY, scale);
  }

  putInChain(
    array: Float64Array,
    lengths: Float64Array,
    pointsOffset: number,
  ): void {
    if (!this.spline) {
      throw new Error("There's no spline.");
    }
    this.spline.putInChain(array, lengths, pointsOffset);
  }
}
