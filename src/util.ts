import sample from 'lodash/sample';
import { Point } from './types';

export function computeCubicCurveLength(
  points: number,
  ax: number,
  ay: number,
  bx: number,
  by: number,
  cx: number,
  cy: number,
  dx: number,
  dy: number,
) {
  const pa = [ax, ay];
  const pb: Point = [0, 0];
  let length = 0;
  let deltaX;
  let deltaY;

  const increment = 1.0 / (points - 1);

  for (let t = increment; t <= 1.0; t += increment) {
    placePoint(pb, t, ax, ay, bx, by, cx, cy, dx, dy);

    deltaX = pa[0] - pb[0];
    deltaY = pa[1] - pb[1];
    length += Math.sqrt(deltaX * deltaX + deltaY * deltaY);

    pa[0] = pb[0];
    pa[1] = pb[1];
  }

  return length;
}

/**
 * Uses the formula from:
 * http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B.C3.A9zier_curves
 */
export function placePoint(
  point: Point,
  t: number,
  ax: number,
  ay: number,
  bx: number,
  by: number,
  cx: number,
  cy: number,
  dx: number,
  dy: number,
): void {
  const oneMinust = 1 - t;
  const oneMinust2 = oneMinust * oneMinust;
  const oneMinust3 = oneMinust2 * oneMinust;
  const t2 = t * t;
  const t3 = t2 * t;

  let f;

  point[0] = oneMinust3 * ax;
  point[1] = oneMinust3 * ay;

  f = 3 * oneMinust2 * t;
  point[0] += f * bx;
  point[1] += f * by;

  f = 3 * oneMinust * t2;
  point[0] += f * cx;
  point[1] += f * cy;

  point[0] += t3 * dx;
  point[1] += t3 * dy;
}

export function randomText(letters: string[], words: number): string {
  let ret = '';

  for (let i = 0; i < words; i++) {
    const wordLength = 2 + Math.floor(Math.random() * 6);

    if (i > 0) {
      ret += ' ';
    }

    for (let j = 0; j < wordLength; j++) {
      ret += sample(letters);
    }
  }

  return ret;
}

export function reverseCurve(curve: string): string {
  const parts = curve.trim().split(/\s+/);
  const ns = parts.slice(1).map((x: string) => +x);

  if (parts[0] === 'c') {
    const [c1x, c1y, c2x, c2y, x, y] = ns;
    return `c ${c2x - x} ${c2y - y} ${c1x - x} ${c1y - y} ${-x} ${-y}`;
  }
  if (parts[0] === 'q') {
    const [x1, y1, x, y] = ns;
    return `q ${x1 - x} ${y1 - y} ${-x} ${-y}`;
  }
  throw new Error('not implemented');
}
