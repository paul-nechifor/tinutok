import Character from './Character';

export default class CharacterSet {
  topTurner: Character;

  bottomTurner: Character;

  minimumSpace: number;

  characters: { [code: string]: Character };

  revCharacters: { [code: string]: Character };

  constructor(
    topTurner: Character,
    bottomTurner: Character,
    minimumSpace: number,
  ) {
    this.topTurner = topTurner;
    this.bottomTurner = bottomTurner;
    this.minimumSpace = minimumSpace;

    this.characters = {};
    this.revCharacters = {};
  }

  putCharacter(code: string, character: Character): void {
    this.characters[code] = character;
    this.revCharacters[code] = character.getInReverse();
  }

  getCharacter(code: string, isInReverse: boolean): Character {
    if (isInReverse) {
      return this.revCharacters[code];
    }
    return this.characters[code];
  }

  characterExists(code: string): boolean {
    return code in this.characters;
  }
}
