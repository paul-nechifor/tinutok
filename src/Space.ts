import Character from './Character';
import BezierSplineBuilder from './BezierSplineBuilder';

export default class Space extends Character {
  constructor(height: number, isInReverse: boolean) {
    super([]);
    this.height = height;
    this.splinePoints = 1;
    this.isInReverse = isInReverse;
  }

  putInChain(
    array: Float64Array,
    lengths: Float64Array,
    pointsOffset: number,
  ): void {
    const builder = new BezierSplineBuilder();
    builder.addQuadratic(
      0,
      0,
      0,
      this.isInReverse ? -this.height : this.height,
    );
    builder.build().putInChain(array, lengths, pointsOffset);
  }
}
