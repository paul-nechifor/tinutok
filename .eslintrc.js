module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: ['airbnb', 'airbnb-typescript/base'],
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ["./tsconfig.json"]
  },
  rules: {
    "@typescript-eslint/no-use-before-define": 0,
    "no-plusplus": 0,
    "no-param-reassign": ['error', { props: false }],
    "prefer-destructuring": ['error', { array: false }],
    "max-len": ["error", { code: 80, ignoreStrings: true}],
    "class-methods-use-this": 0,
    "import/prefer-default-export": 0,
  }
}
