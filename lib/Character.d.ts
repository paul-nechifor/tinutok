import BezierSpline from './BezierSpline';
export default class Character {
    commands: string[];
    spline: BezierSpline | null;
    isInReverse: boolean;
    height: number;
    splinePoints: number;
    constructor(commands: string[]);
    getInReverse(): Character;
    draw(ctx: CanvasRenderingContext2D, startX: number, startY: number, scale: number): void;
    putInChain(array: Float64Array, lengths: Float64Array, pointsOffset: number): void;
}
