"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const BezierSpline_1 = __importDefault(require("./BezierSpline"));
const util_1 = require("./util");
const LENGTH_COMPUTING_POINTS = 1000;
class BezierSplineBuilder {
    constructor() {
        this.array = [];
    }
    build() {
        const v = this.array;
        let length = 0;
        const lengths = [];
        let height = 0;
        for (let i = 0, k = 0, len = v.length; i < len; i += 6, k++) {
            lengths[k] = (0, util_1.computeCubicCurveLength)(LENGTH_COMPUTING_POINTS, 0, 0, v[i + 0], v[i + 1], v[i + 2], v[i + 3], v[i + 4], v[i + 5]);
            length += lengths[k];
            height += v[i + 5];
        }
        const typedArray = new Float64Array(v);
        const typedLengths = new Float64Array(lengths);
        return new BezierSpline_1.default(typedArray, length, typedLengths, height);
    }
    addCubic(c1x, c1y, c2x, c2y, tx, ty) {
        this.array.push(c1x, c1y, c2x, c2y, tx, ty);
    }
    addMatchingCubic(c2x, c2y, tx, ty) {
        if (this.array.length === 0) {
            throw new Error('Cannot be first');
        }
        const v = this.array;
        const n = v.length;
        v.push(v[n - 2] - v[n - 4], v[n - 1] - v[n - 3], c2x, c2y, tx, ty);
    }
    // From: http://stackoverflow.com/questions/3162645
    addQuadratic(cx, cy, tx, ty) {
        const f = 2.0 / 3.0;
        this.array.push(f * cx, f * cy, tx + f * (cx - tx), ty + f * (cy - ty), tx, ty);
    }
}
exports.default = BezierSplineBuilder;
