import CharacterSet from './CharacterSet';
export default function buildCharacterSet(): CharacterSet;
