import Character from './Character';
export default class Space extends Character {
    constructor(height: number, isInReverse: boolean);
    putInChain(array: Float64Array, lengths: Float64Array, pointsOffset: number): void;
}
