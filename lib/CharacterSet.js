"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CharacterSet {
    constructor(topTurner, bottomTurner, minimumSpace) {
        this.topTurner = topTurner;
        this.bottomTurner = bottomTurner;
        this.minimumSpace = minimumSpace;
        this.characters = {};
        this.revCharacters = {};
    }
    putCharacter(code, character) {
        this.characters[code] = character;
        this.revCharacters[code] = character.getInReverse();
    }
    getCharacter(code, isInReverse) {
        if (isInReverse) {
            return this.revCharacters[code];
        }
        return this.characters[code];
    }
    characterExists(code) {
        return code in this.characters;
    }
}
exports.default = CharacterSet;
