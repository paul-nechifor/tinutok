"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Word = exports.drawOnCanvas = exports.getSvg = exports.randomText = exports.placePoint = exports.computeCubicCurveLength = exports.Text = exports.Space = exports.Character = exports.CharacterSet = exports.buildCharacterSet = exports.BezierSplineWalker = exports.BezierSpline = exports.BezierSplineSpeedWalker = exports.BezierSplineBuilder = void 0;
var BezierSplineBuilder_1 = require("./BezierSplineBuilder");
Object.defineProperty(exports, "BezierSplineBuilder", { enumerable: true, get: function () { return __importDefault(BezierSplineBuilder_1).default; } });
var BezierSplineSpeedWalker_1 = require("./BezierSplineSpeedWalker");
Object.defineProperty(exports, "BezierSplineSpeedWalker", { enumerable: true, get: function () { return __importDefault(BezierSplineSpeedWalker_1).default; } });
var BezierSpline_1 = require("./BezierSpline");
Object.defineProperty(exports, "BezierSpline", { enumerable: true, get: function () { return __importDefault(BezierSpline_1).default; } });
var BezierSplineWalker_1 = require("./BezierSplineWalker");
Object.defineProperty(exports, "BezierSplineWalker", { enumerable: true, get: function () { return __importDefault(BezierSplineWalker_1).default; } });
var buildCharacterSet_1 = require("./buildCharacterSet");
Object.defineProperty(exports, "buildCharacterSet", { enumerable: true, get: function () { return __importDefault(buildCharacterSet_1).default; } });
var CharacterSet_1 = require("./CharacterSet");
Object.defineProperty(exports, "CharacterSet", { enumerable: true, get: function () { return __importDefault(CharacterSet_1).default; } });
var Character_1 = require("./Character");
Object.defineProperty(exports, "Character", { enumerable: true, get: function () { return __importDefault(Character_1).default; } });
var Space_1 = require("./Space");
Object.defineProperty(exports, "Space", { enumerable: true, get: function () { return __importDefault(Space_1).default; } });
var Text_1 = require("./Text");
Object.defineProperty(exports, "Text", { enumerable: true, get: function () { return __importDefault(Text_1).default; } });
var util_1 = require("./util");
Object.defineProperty(exports, "computeCubicCurveLength", { enumerable: true, get: function () { return util_1.computeCubicCurveLength; } });
Object.defineProperty(exports, "placePoint", { enumerable: true, get: function () { return util_1.placePoint; } });
Object.defineProperty(exports, "randomText", { enumerable: true, get: function () { return util_1.randomText; } });
var out_1 = require("./out");
Object.defineProperty(exports, "getSvg", { enumerable: true, get: function () { return out_1.getSvg; } });
Object.defineProperty(exports, "drawOnCanvas", { enumerable: true, get: function () { return out_1.drawOnCanvas; } });
var Word_1 = require("./Word");
Object.defineProperty(exports, "Word", { enumerable: true, get: function () { return __importDefault(Word_1).default; } });
