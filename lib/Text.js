"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const BezierSpline_1 = __importDefault(require("./BezierSpline"));
const Word_1 = __importDefault(require("./Word"));
const Space_1 = __importDefault(require("./Space"));
const constants_1 = require("./constants");
const epsilon = 0.00000001;
/**
 * Compute the number of points that are needed for a spline to keep all of
 * these elements.
 */
function getNumberOfSplinePoints(elements) {
    let ret = 0;
    for (let i = 0, len = elements.length; i < len; i++) {
        ret += elements[i].splinePoints;
    }
    return ret;
}
function sumWordsHeight(words) {
    let sum = 0;
    for (let i = 0, len = words.length; i < len; i++) {
        sum += words[i].height;
    }
    return sum;
}
function getStringWords(string) {
    // TODO: split into parts correctly.
    return string.split(/\s+/);
}
class Text {
    constructor(characterSet, string, wrapHeight = -1) {
        this.characterSet = characterSet;
        this.width = -1;
        this.wrapHeight = wrapHeight;
        this.wrap = this.wrapHeight > 0;
        this.height = this.wrap ? this.wrapHeight : Number.MAX_VALUE;
        const stringWords = getStringWords(string);
        this.words = this.getWords(stringWords);
        this.index = 0;
        this.elements = this.fillElements();
    }
    getWords(textWords) {
        const usableHeight = this.getUsableHeight(2);
        const characterSet = this.characterSet;
        const ret = [];
        for (const textWord of textWords) {
            let letters = [];
            let accumulatedHeight = 0;
            for (const letter of textWord) {
                const letterHeight = characterSet.getCharacter(letter, false).height;
                if (accumulatedHeight + letterHeight > usableHeight) {
                    ret.push(new Word_1.default(letters.join(''), characterSet));
                    letters = [];
                    accumulatedHeight = 0;
                }
                letters.push(letter);
                accumulatedHeight += letterHeight;
            }
            if (letters.length) {
                ret.push(new Word_1.default(letters.join(''), characterSet));
            }
        }
        return ret;
    }
    getUsableHeight(turnersForLine) {
        return this.height - turnersForLine * constants_1.turnerHeight;
    }
    fillElements() {
        const elements = [];
        let lineNr = 0;
        for (;;) {
            const turnersForLine = lineNr === 0 ? 2 : 1;
            const usableHeight = this.getUsableHeight(turnersForLine);
            const { success, words: lineWords, usedHeight, } = this.extractWords(usableHeight);
            if (!success) {
                break;
            }
            if (!this.wrap) {
                this.height = usedHeight;
            }
            if (lineNr > 0) {
                elements.push(lineNr % 2 === 0
                    ? this.characterSet.topTurner
                    : this.characterSet.bottomTurner);
            }
            const isInReverse = lineNr % 2 === 1;
            const lineWordsHeight = sumWordsHeight(lineWords);
            for (let i = 0, len = lineWords.length; i < len; i++) {
                if (this.wrap) {
                    if (i > 0) {
                        const nSpaces = lineWords.length - 1;
                        const spaceH = (this.height - lineWordsHeight) / nSpaces;
                        if (spaceH > epsilon) {
                            elements.push(new Space_1.default(spaceH, isInReverse));
                        }
                    }
                    else if (i === 0 && lineWords.length === 1) {
                        const spaceH = this.height - lineWordsHeight;
                        if (spaceH > epsilon) {
                            elements.push(new Space_1.default(spaceH, isInReverse));
                        }
                    }
                }
                lineWords[i].pushIntoElements(elements, isInReverse);
            }
            lineNr++;
        }
        this.width = 8 * lineNr;
        return elements;
    }
    extractWords(maxHeight) {
        let usedHeight = 0;
        const minimumSpace = this.characterSet.minimumSpace;
        const extracted = [];
        for (;;) {
            if (this.index >= this.words.length) {
                if (extracted.length === 0) {
                    return { success: false, words: [], usedHeight: 0 };
                }
                return { success: true, words: extracted, usedHeight };
            }
            const word = this.words[this.index];
            if (extracted.length > 0) {
                usedHeight += minimumSpace;
            }
            usedHeight += word.height;
            if (usedHeight >= maxHeight) {
                return { success: true, words: extracted, usedHeight };
            }
            extracted.push(word);
            this.index++;
        }
    }
    /**
     * Compute and return the BezierSpline for this text.
     */
    getSpline() {
        const totalPoints = getNumberOfSplinePoints(this.elements);
        const array = new Float64Array(totalPoints * 6);
        const length = -1; // This is also irrelevant... possibly...
        const lengths = new Float64Array(totalPoints);
        let height = 0;
        let pointsOffset = 0;
        let element;
        let points;
        for (let i = 0, len = this.elements.length; i < len; i++) {
            element = this.elements[i];
            points = element.splinePoints;
            element.putInChain(array, lengths, pointsOffset);
            pointsOffset += points;
            height += element.height;
        }
        return new BezierSpline_1.default(array, length, lengths, height);
    }
}
exports.default = Text;
