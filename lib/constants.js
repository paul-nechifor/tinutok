"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.turnerWidth = exports.turnerHeight = exports.textSize = void 0;
exports.textSize = 4;
exports.turnerHeight = exports.textSize;
exports.turnerWidth = 2 * exports.textSize;
