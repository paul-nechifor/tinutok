import Character from './Character';
export default class CharacterSet {
    topTurner: Character;
    bottomTurner: Character;
    minimumSpace: number;
    characters: {
        [code: string]: Character;
    };
    revCharacters: {
        [code: string]: Character;
    };
    constructor(topTurner: Character, bottomTurner: Character, minimumSpace: number);
    putCharacter(code: string, character: Character): void;
    getCharacter(code: string, isInReverse: boolean): Character;
    characterExists(code: string): boolean;
}
