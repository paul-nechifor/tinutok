"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const BezierSplineBuilder_1 = __importDefault(require("./BezierSplineBuilder"));
const util_1 = require("./util");
class Character {
    constructor(commands) {
        this.commands = commands;
        this.spline = null;
        this.isInReverse = false;
        this.height = 0;
        this.splinePoints = 0;
        this.splinePoints = commands.length;
        const builder = new BezierSplineBuilder_1.default();
        for (let i = 0, len = commands.length; i < len; i++) {
            const command = commands[i];
            const parts = command.split(/\s+/);
            const numbers = parts
                .slice(1, parts.length)
                .map((e) => Number(e));
            switch (parts[0]) {
                case 'c':
                    builder.addCubic(...numbers);
                    break;
                case 'mc':
                    builder.addMatchingCubic(...numbers);
                    break;
                case 'q':
                    builder.addQuadratic(...numbers);
                    break;
                default:
                    throw new Error('Forgot to implement it.');
                    break;
            }
        }
        this.spline = builder.build();
        this.height = this.spline.height;
    }
    getInReverse() {
        const c = new Character(this.commands.reverse().map(util_1.reverseCurve));
        c.isInReverse = true;
        return c;
    }
    draw(ctx, startX, startY, scale) {
        if (!this.spline) {
            throw new Error("There's no spline.");
        }
        this.spline.draw(ctx, startX, startY, scale);
    }
    putInChain(array, lengths, pointsOffset) {
        if (!this.spline) {
            throw new Error("There's no spline.");
        }
        this.spline.putInChain(array, lengths, pointsOffset);
    }
}
exports.default = Character;
