import BezierSpline from './BezierSpline';
export default class BezierSplineBuilder {
    array: number[];
    constructor();
    build(): BezierSpline;
    addCubic(c1x: number, c1y: number, c2x: number, c2y: number, tx: number, ty: number): void;
    addMatchingCubic(c2x: number, c2y: number, tx: number, ty: number): void;
    addQuadratic(cx: number, cy: number, tx: number, ty: number): void;
}
