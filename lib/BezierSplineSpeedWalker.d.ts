import BezierSplineWalker from './BezierSplineWalker';
import { Point } from './types';
export default class BezierSplineSpeedWalker {
    bezierSplineWalker: BezierSplineWalker;
    pointA: Point;
    pointB: Point;
    linePos: number;
    lineLength: number;
    dx: number;
    dy: number;
    x: number;
    y: number;
    r: number;
    constructor(bezierSplineWalker: BezierSplineWalker);
    advanceBy(ammount: number): boolean;
}
