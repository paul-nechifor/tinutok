"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Character_1 = __importDefault(require("./Character"));
const BezierSplineBuilder_1 = __importDefault(require("./BezierSplineBuilder"));
class Space extends Character_1.default {
    constructor(height, isInReverse) {
        super([]);
        this.height = height;
        this.splinePoints = 1;
        this.isInReverse = isInReverse;
    }
    putInChain(array, lengths, pointsOffset) {
        const builder = new BezierSplineBuilder_1.default();
        builder.addQuadratic(0, 0, 0, this.isInReverse ? -this.height : this.height);
        builder.build().putInChain(array, lengths, pointsOffset);
    }
}
exports.default = Space;
