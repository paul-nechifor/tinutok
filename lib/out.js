"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.drawOnCanvas = exports.getSvg = void 0;
const buildCharacterSet_1 = __importDefault(require("./buildCharacterSet"));
const Text_1 = __importDefault(require("./Text"));
const constants_1 = require("./constants");
const BezierSplineWalker_1 = __importDefault(require("./BezierSplineWalker"));
const BezierSplineSpeedWalker_1 = __importDefault(require("./BezierSplineSpeedWalker"));
function getSvg({ string, characterSet, strokeWidth, style = '', scale = 4, padding = 10, wrapHeight = -1, noBackground = false, addSize = false, }) {
    const givenHeight = (wrapHeight - 2 * padding) / scale - 2 * constants_1.textSize;
    const set = characterSet || (0, buildCharacterSet_1.default)();
    const text = new Text_1.default(set, processString(string, set), givenHeight);
    const spline = text.getSpline();
    const startX = padding + constants_1.textSize * scale;
    const startY = padding + constants_1.textSize * scale;
    const path = spline.getRelativeSvgPath(startX, startY, scale);
    const height = scale * text.height + 2 * padding + 2 * scale * constants_1.textSize;
    const width = text.width * scale + 2 * padding;
    const svg = [
        '<svg xmlns="http://www.w3.org/2000/svg" ',
        `viewBox="0 0 ${width} ${height}"`,
        addSize ? ` width="${width}" height="${height}"` : '',
        '>',
        noBackground
            ? ''
            : `<rect width="${width}" height="${height}" fill="#eee"/>`,
        `<path d="${path}" `,
        `style="fill: none; stroke: black; stroke-width: ${strokeWidth || scale}px; `,
        `${style}"/>`,
        '</svg>',
    ].join('');
    return {
        svg,
        text,
        width,
        height,
        spline,
        startX,
        startY,
    };
}
exports.getSvg = getSvg;
function processString(string, characterSet) {
    const characters = Object.keys(characterSet.characters);
    const letters = string
        .toLowerCase()
        .replace(/â/g, 'î')
        .split('')
        .filter((x) => x === ' ' || characters.includes(x))
        .join('');
    return letters;
}
function drawOnCanvas({ canvas, string, characterSet, scale = 4, padding, wrapHeight, speedAdvance = 0.1, speedSteps = 5, nibWidth = 3, nibHeight = 1, showPath = true, }) {
    const { width, height, spline, startX, startY } = getSvg({
        string,
        characterSet,
        scale,
        padding,
        wrapHeight,
    });
    const ctx = canvas.getContext('2d');
    if (!ctx) {
        throw new Error('No canvas.');
    }
    canvas.setAttribute('width', `${width}`);
    canvas.setAttribute('height', `${height}`);
    ctx.fillStyle = '#EEEEEE';
    ctx.fillRect(0, 0, width, height);
    if (showPath) {
        ctx.lineWidth = 1;
        spline.draw(ctx, startX, startY, scale);
    }
    const walker = new BezierSplineWalker_1.default(spline, speedAdvance, startX, startY, scale);
    const sWalker = new BezierSplineSpeedWalker_1.default(walker);
    let morePoints = true;
    function doNextPoint() {
        if (morePoints) {
            setTimeout(doNextPoint, 0);
        }
        for (let i = 0; i < speedSteps && morePoints; i++) {
            const { x, y } = sWalker;
            const len = nibWidth;
            ctx.lineWidth = nibHeight;
            ctx.beginPath();
            ctx.moveTo(x - len, y);
            ctx.lineTo(x + len, y + len);
            ctx.stroke();
            morePoints = sWalker.advanceBy(speedAdvance);
        }
    }
    doNextPoint();
    return () => {
        morePoints = false;
    };
}
exports.drawOnCanvas = drawOnCanvas;
