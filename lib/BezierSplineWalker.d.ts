import { Point } from './types';
import BezierSpline from './BezierSpline';
export default class BezierSplineWalker {
    bezierSpline: BezierSpline;
    ammount: number;
    lastX: number;
    lastY: number;
    scale: number;
    index: number;
    increment: number;
    t: number;
    arrayIndex: number;
    curveNumbers: Float64Array;
    constructor(bezierSpline: BezierSpline, ammount: number, startX: number, startY: number, scale: number);
    /**
     * Advances to the new point and returns true or returns false if there are no
     * more points. Needs to be called before the first point is requested.
     */
    next(): boolean;
    getPoint(point: Point): void;
}
