import { Point } from './types';
export declare function computeCubicCurveLength(points: number, ax: number, ay: number, bx: number, by: number, cx: number, cy: number, dx: number, dy: number): number;
/**
 * Uses the formula from:
 * http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B.C3.A9zier_curves
 */
export declare function placePoint(point: Point, t: number, ax: number, ay: number, bx: number, by: number, cx: number, cy: number, dx: number, dy: number): void;
export declare function randomText(letters: string[], words: number): string;
export declare function reverseCurve(curve: string): string;
