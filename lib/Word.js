"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A Word is an array of Characters.
 */
class Word {
    constructor(codes, characterSet, isInReverse) {
        this.codes = codes;
        this.characterSet = characterSet;
        this.isInReverse = isInReverse !== undefined ? isInReverse : false;
        this.elements = this.computeElements();
        this.height = this.computeHeight();
    }
    computeElements() {
        const elements = [];
        const cSet = this.characterSet;
        let character;
        for (let i = 0, len = this.codes.length; i < len; i++) {
            character = cSet.getCharacter(this.codes[i], this.isInReverse);
            if (character === undefined) {
                throw new Error('No such character');
            }
            elements.push(character);
        }
        return elements;
    }
    computeHeight() {
        let height = 0;
        for (let i = 0, len = this.elements.length; i < len; i++) {
            height += this.elements[i].height;
        }
        return height;
    }
    getWordInReverse() {
        return new Word(this.codes, this.characterSet, !this.isInReverse);
    }
    pushIntoElements(elements, isInReverse) {
        const cSet = this.characterSet;
        let i;
        let len;
        if (isInReverse) {
            for (i = 0, len = this.elements.length; i < len; i++) {
                elements.push(cSet.getCharacter(this.codes[i], isInReverse));
            }
        }
        else {
            for (i = 0, len = this.elements.length; i < len; i++) {
                elements.push(this.elements[i]);
            }
        }
    }
}
exports.default = Word;
