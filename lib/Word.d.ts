import CharacterSet from './CharacterSet';
import Character from './Character';
/**
 * A Word is an array of Characters.
 */
export default class Word {
    codes: string;
    characterSet: CharacterSet;
    isInReverse: boolean;
    elements: Character[];
    height: number;
    constructor(codes: string, characterSet: CharacterSet, isInReverse?: boolean);
    computeElements(): Character[];
    computeHeight(): number;
    getWordInReverse(): Word;
    pushIntoElements(elements: Character[], isInReverse: boolean): void;
}
