export default class BezierSpline {
    array: Float64Array;
    length: number;
    lengths: Float64Array;
    height: number;
    constructor(array: Float64Array, length: number, lengths: Float64Array, height: number);
    putInChain(array: Float64Array, lengths: Float64Array, pointsOffset: number): void;
    draw(ctx: CanvasRenderingContext2D, startX: number, startY: number, scale?: number): void;
    getRelativeSvgPath(startX: number, startY: number, scale?: number): string;
}
