"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reverseCurve = exports.randomText = exports.placePoint = exports.computeCubicCurveLength = void 0;
const sample_1 = __importDefault(require("lodash/sample"));
function computeCubicCurveLength(points, ax, ay, bx, by, cx, cy, dx, dy) {
    const pa = [ax, ay];
    const pb = [0, 0];
    let length = 0;
    let deltaX;
    let deltaY;
    const increment = 1.0 / (points - 1);
    for (let t = increment; t <= 1.0; t += increment) {
        placePoint(pb, t, ax, ay, bx, by, cx, cy, dx, dy);
        deltaX = pa[0] - pb[0];
        deltaY = pa[1] - pb[1];
        length += Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        pa[0] = pb[0];
        pa[1] = pb[1];
    }
    return length;
}
exports.computeCubicCurveLength = computeCubicCurveLength;
/**
 * Uses the formula from:
 * http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Cubic_B.C3.A9zier_curves
 */
function placePoint(point, t, ax, ay, bx, by, cx, cy, dx, dy) {
    const oneMinust = 1 - t;
    const oneMinust2 = oneMinust * oneMinust;
    const oneMinust3 = oneMinust2 * oneMinust;
    const t2 = t * t;
    const t3 = t2 * t;
    let f;
    point[0] = oneMinust3 * ax;
    point[1] = oneMinust3 * ay;
    f = 3 * oneMinust2 * t;
    point[0] += f * bx;
    point[1] += f * by;
    f = 3 * oneMinust * t2;
    point[0] += f * cx;
    point[1] += f * cy;
    point[0] += t3 * dx;
    point[1] += t3 * dy;
}
exports.placePoint = placePoint;
function randomText(letters, words) {
    let ret = '';
    for (let i = 0; i < words; i++) {
        const wordLength = 2 + Math.floor(Math.random() * 6);
        if (i > 0) {
            ret += ' ';
        }
        for (let j = 0; j < wordLength; j++) {
            ret += (0, sample_1.default)(letters);
        }
    }
    return ret;
}
exports.randomText = randomText;
function reverseCurve(curve) {
    const parts = curve.trim().split(/\s+/);
    const ns = parts.slice(1).map((x) => +x);
    if (parts[0] === 'c') {
        const [c1x, c1y, c2x, c2y, x, y] = ns;
        return `c ${c2x - x} ${c2y - y} ${c1x - x} ${c1y - y} ${-x} ${-y}`;
    }
    if (parts[0] === 'q') {
        const [x1, y1, x, y] = ns;
        return `q ${x1 - x} ${y1 - y} ${-x} ${-y}`;
    }
    throw new Error('not implemented');
}
exports.reverseCurve = reverseCurve;
