import BezierSpline from './BezierSpline';
import CharacterSet from './CharacterSet';
import Character from './Character';
import Word from './Word';
interface Extracted {
    success: boolean;
    words: Word[];
    usedHeight: number;
}
export default class Text {
    characterSet: CharacterSet;
    wrapHeight: number;
    height: number;
    wrap: boolean;
    width: number;
    words: Word[];
    index: number;
    elements: Character[];
    constructor(characterSet: CharacterSet, string: string, wrapHeight?: number);
    getWords(textWords: string[]): Word[];
    getUsableHeight(turnersForLine: number): number;
    fillElements(): Character[];
    extractWords(maxHeight: number): Extracted;
    /**
     * Compute and return the BezierSpline for this text.
     */
    getSpline(): BezierSpline;
}
export {};
