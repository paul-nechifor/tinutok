import CharacterSet from './CharacterSet';
import Text from './Text';
import BezierSpline from './BezierSpline';
interface SvgOpts {
    string: string;
    characterSet?: CharacterSet;
    strokeWidth?: number;
    style?: string;
    scale?: number;
    padding?: number;
    wrapHeight?: number;
    noBackground?: boolean;
    addSize?: boolean;
}
interface SvgOut {
    svg: string;
    text: Text;
    width: number;
    height: number;
    spline: BezierSpline;
    startX: number;
    startY: number;
}
export declare function getSvg({ string, characterSet, strokeWidth, style, scale, padding, wrapHeight, noBackground, addSize, }: SvgOpts): SvgOut;
interface DrawOpts {
    canvas: HTMLCanvasElement;
    string: string;
    characterSet?: CharacterSet;
    scale?: number;
    padding?: number;
    wrapHeight?: number;
    speedAdvance?: number;
    speedSteps?: number;
    nibWidth?: number;
    nibHeight?: number;
    showPath?: boolean;
}
export declare function drawOnCanvas({ canvas, string, characterSet, scale, padding, wrapHeight, speedAdvance, speedSteps, nibWidth, nibHeight, showPath, }: DrawOpts): () => void;
export {};
