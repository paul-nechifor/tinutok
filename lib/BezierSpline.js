"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BezierSpline {
    constructor(array, length, lengths, height) {
        this.array = array;
        this.length = length;
        this.lengths = lengths;
        this.height = height;
    }
    putInChain(array, lengths, pointsOffset) {
        let i;
        for (i = 0; i < this.array.length; i++) {
            array[pointsOffset * 6 + i] = this.array[i];
        }
        for (i = 0; i < this.lengths.length; i++) {
            lengths[pointsOffset + i] = this.lengths[i];
        }
    }
    draw(ctx, startX, startY, scale) {
        const s = scale !== undefined ? scale : 1;
        ctx.beginPath();
        ctx.moveTo(startX, startY);
        const v = this.array;
        let lastX = startX;
        let lastY = startY;
        for (let i = 0, len = v.length; i < len; i += 6) {
            ctx.bezierCurveTo(lastX + v[i + 0] * s, lastY + v[i + 1] * s, lastX + v[i + 2] * s, lastY + v[i + 3] * s, lastX + v[i + 4] * s, lastY + v[i + 5] * s);
            lastX += v[i + 4] * s;
            lastY += v[i + 5] * s;
        }
        ctx.stroke();
    }
    getRelativeSvgPath(startX, startY, scale) {
        const s = scale !== undefined ? scale : 1;
        const v = this.array;
        const points = [];
        for (let i = 0, len = v.length; i < len; i += 6) {
            points.push(v[i + 0] * s);
            points.push(v[i + 1] * s);
            points.push(v[i + 2] * s);
            points.push(v[i + 3] * s);
            points.push(v[i + 4] * s);
            points.push(v[i + 5] * s);
        }
        const ret = `M${startX} ${startY}c${points.join(' ')}`;
        return ret;
    }
}
exports.default = BezierSpline;
